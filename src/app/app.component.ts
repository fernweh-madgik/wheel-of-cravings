import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import * as Two from './two.js';
import { faGlobeEurope, faSync } from '@fortawesome/free-solid-svg-icons';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ResultsModalComponent } from './results-modal/results-modal.component.js';
import { ActivatedRoute, Router, RoutesRecognized } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {

  readonly faGlobe = faGlobeEurope;

  modalRef: NgbModalRef;

  _lang: 'en' | 'ar' = 'en';

  showLangBtn = true;
  get lang() {
    return this._lang;
  }

  set lang(lang: 'en' | 'ar') {
    this._lang = lang;
    this.wheel.setWords(this.getWords());
    this.wheel.drawWheel();
  }

  readonly faSync = faSync;
  private wheel;
  wordIndexes: number[] = [];

  readonly WORDS = [
    {
      en: {
        word: 'Chillies',
        text: `The body needs to cool down.
        Generally safe for women to eat in
        moderation, might cause heartburn or
        indigestion.`
      },
      ar: {
        word: 'الفلفل الحار',
        text: `.يحتاج الجسم إلى التبريد. لذلك يعد تناوله بإعتدال آمناً بشكل عام، ولكنه قد يسبب حرقة المعدة أو عسر في الهضم`
      }
    },
    {
      en: {
        word: 'Ice cream',
        text: `Craving related to cooling
        benefits and sweetness. Eat in moderation
        or switch to low fat frozen yoghurt.`
      },
      ar: {
        word: 'آيس كريم',
        text: `.وحم متعلق بفوائد التبريد والحلاوة. تناوليه باعتدال أو استبدليه بالزبادي المثلج قليل الدسم`
      }
    },
    {
      en: {
        word: 'Chocolate',
        text: `Craved because it is sweet and high in calcium. Might be linked
         to low calcium levels. Good alternative is low fat frozen yogurt.`
      },
      ar: {
        word: 'شوكولاتة',
        text: `.مشتهى لأنه حلو وغني بالكالسيوم. قد يرتبط ذلك بإنخفاض مستويات الكالسيوم في الجسم. البديل الجيد هو الزبادي المثلج قليل الدسم`
      }
    },
    {
      en: {
        word: 'Chips',
        text: `Good alternative is pickles: it might be because it is high in
         sodium. Safe option to eat.`
      },
      ar: {
        word: 'رقائق البطاطا',
        text: `.البديل المناسب هي المخللات لأنها تحتوي على نسبة عالية من الصوديوم. خيار أفضل للأكل`
      }
    },
    {
      en: {
        word: 'Red meat',
        text: `It is high in iron. Might be because the woman is iron deficient.
         Moderation required and supplement with greens.`
      },
      ar: {
        word: 'اللحوم الحمراء',
        text: `.تحتوي على نسبة عالية من الحديد. فالمرأة قد تعاني من نقص في الحديد. والاعتدال مطلوب مع تناول الخضراوات`
      }
    },
    {
      en: {
        word: 'Peanut butter',
        text: `Satisfies the need for comfort food. A quick tip is to spread it
         on fruit to avoid eating the whole jar.`
      },
      ar: {
        word: 'زبدة الفول السوداني',
        text: `.ترضي الحاجة إلى طعام سهل. نصيحة صغيرة هي ضرورة تناولها مع الفاكهة لتجنب تناول الوعاء بأكمله`
      }
    },
    {
      en: {
        word: 'Ice',
        text: `Can be linked to iron deficiency and anemia. Incorporate more
         protein in to your diet.`
      },
      ar: {
        word: 'الثلج',
        text: `.يمكن أن يرتبط بنقص الحديد وفقر الدم. أدخلي المزيد من البروتين في نظامك الغذائي`
      }
    },
    {
      en: {
        word: 'Hair',
        text: `Related to Pica. Not safe to consume and does not have any
         nutritional value. Talk to your doctor about this issue as you might
          have an iron deficiency.`
      },
      ar: {
        word: 'الشعر',
        text: `.متعلّق بمتلازمة بيكا، وهو غير آمن للاستهلاك وليس له أي قيمة غذائية. تحدّثي إلى طبيبك حول هذه المشكلة فقد يكون لديك نقص في الحديد`
      }
    },
    {
      en: {
        word: 'Paper',
        text: `Craving related to Pica. Not safe to consume and does not have
         any nutritional value. Talk to your doctor about this issue as you
          might have an iron deficiency.`
      },
      ar: {
        word: 'الورق',
        text: `.متعلّق بمتلازمة بيكا، غير آمن للاستهلاك وليس له أي قيمة غذائية. تحدثي إلى طبيبك حول هذه المشكلة فقد يكون لديك نقص في الحديد`
      }
    },
    {
      en: {
        word: 'Glass',
        text: `Craving related to Pica. Not safe to consume and does not have
         any nutritional value. Talk to your doctor about this issue as you
          might have an iron deficiency.`
      },
      ar: {
        word: 'الزجاج',
        text: `.متعلّق بمتلازمة بيكا، غير آمن للاستهلاك وليس له أي قيمة غذائية. تحدثي إلى طبيبك حول هذه المشكلة فقد يكون لديك نقص في الحديد`
      }
    },
    {
      en: {
        word: 'Paint',
        text: `Craving related to Pica. Not safe to consume and does not have
         any nutritional value. Talk to your doctor about this issue as you
          might have an iron deficiency.`
      },
      ar: {
        word: 'الطلاء',
        text: `.متعلّق بمتلازمة بيكا، غير آمن للاستهلاك وليس له أي قيمة غذائية. تحدثي إلى طبيبك حول هذه المشكلة فقد يكون لديك نقص في الحديد`
      }
    },
    {
      en: {
        word: 'Stones',
        text: `Craving related to Pica. Talk to your doctor about this issue as
         it might mean that you are lacking nutrients like iron, magnesium and
          zinc. Potential substitute: sugarless gum.`
      },
      ar: {
        word: 'الرغبة في الحجارة',
        text: `.متعلّق بمتلازمة بيكا، تحدثي إلى طبيبك حول هذه المشكلة لأنها قد تعني أنك تفتقرين إلى العناصر الغذائية كالحديد والمغنيسيوم والزنك. البديل المحتمل: علكة خالية من السكر`
      }
    }
  ];

  constructor(private modalService: NgbModal, private router: Router) { }

  ngOnInit(): void {

    this.router.events.subscribe((data) => {
      if (data instanceof RoutesRecognized) {
        const params = (data.state as any)._root.value.queryParams;
        if (params['lang']) {
          this.lang = params['lang'];
          console.log(this.lang);
          this.showLangBtn = false;
        }
      }
    });

    const COLORS = [
      '#445b55',
      '#b2bfae',
      '#6c3349',
      '#f1f1f1',
      // '#95915e',
      // '#c8a27a',
      // '#afbfac',
      // '#efefef',
      // '#e8cb81'
    ];

    const TICKER_COLOR = '#eaeaea';
    const PI = Math.PI;

    const degToRad = deg => deg / 180 * PI;

    const getCoordOnCircle = (r, angleInRad, { cx, cy }) => {
      return {
        x: cx + r * Math.cos(angleInRad),
        y: cy + r * Math.sin(angleInRad),
      };
    };

    const wheelFactory = (mountElem) => {
      if (!mountElem || !('nodeType' in mountElem)) {
        throw new Error('no mount element provided');
      }

      const eventMap = {
        mousedown: handleCursorDown,
        touchstart: handleCursorDown,
        mousemove: handleCursorMove,
        touchmove: handleCursorMove,
        mouseup: handleCursorUp,
        mouseleave: handleCursorUp,
        touchend: handleCursorUp
      };
      const ratios = {
        tickerRadius: 0.06, // of width
        textSize: () => 0.1, //this.lang === 'en' ? 0.09 : 0.011, // of radius
        edgeDist: -0.25, // of radius
      };
      let options: {
        width: number,
        height: number,
        type: string,
        onWheelTick?: (angle) => void,
        onWheelStop?: () => void
      } = {
        width: 360,
        height: 360,
        type: 'svg',
      };
      const friction = 0.95;
      const maxSpeed = 0.5;
      let isGroupActive = false;
      let curPosArr = [];
      let dirScalar = 1;
      let lastCurTime;
      let speed;
      let words;
      let two;
      let group;

      function init(opts) {
        options = { ...options, ...opts };
        two = new Two({
          type: Two.Types[options.type],
          width: options.width,
          height: options.height,
        })
          .bind('resize', handleResize)
          .play();

        initEvents();
        two.appendTo(mountElem);
        setViewBox(options.width, options.height);
        two.renderer.domElement.setAttribute(
          'style',
          `
        -moz-user-select:none;
        -ms-user-select:none;
        -webkit-user-select:none;
        user-select:none;
        -webkit-tap-highlight-color: rgba(0,0,0,0);
        position: absolute;
        height: 100%;
        width: 100%;
        left: 0;
        top: 0;
      `
        );
      }

      function setWords(wordsArr) {
        words = wordsArr;
      }

      function setViewBox(width, height) {
        two.renderer.domElement.setAttribute('viewBox', `0 0 ${width} ${height}`);
      }

      function drawTicker() {
        const { width } = two;
        const outerRadius = ratios.tickerRadius * width;

        const tickerCircleShadow = drawTickerCircle(outerRadius, -5, 3, '#9f9f9f');
        const circleCenterShadow = tickerCircleShadow.translation;
        drawTickerArrow(outerRadius, degToRad(30), circleCenterShadow, '#9f9f9f');

        const tickerCircle = drawTickerCircle(outerRadius);
        const circleCenter = tickerCircle.translation;
        drawTickerArrow(outerRadius, degToRad(30), circleCenter);
      }

      function drawTickerCircle(outerRadius, ox = 0, oy = 0, color = TICKER_COLOR) {
        const { height, width } = two;
        const arc = two.makeArcSegment(
          width - outerRadius + ox,
          height / 2 + oy,
          outerRadius,
          outerRadius * 0.5,
          0,
          2 * PI
        );

        arc.noStroke();
        arc.fill = color;
        return arc;
      }

      function drawTickerArrow(radius, tangentAngle, tickerCenter, color = TICKER_COLOR) {
        const { x, y } = tickerCenter;

        const pointA = getCoordOnCircle(radius, PI, { cx: x, cy: y });
        const pointB = getCoordOnCircle(radius, PI / 2 + tangentAngle, {
          cx: x,
          cy: y,
        });
        const pointC = {
          x: x - radius / Math.cos(PI / 2 - tangentAngle),
          y,
        };
        const pointD = getCoordOnCircle(radius, 3 * PI / 2 - tangentAngle, {
          cx: x,
          cy: y,
        });
        const path = two.makePath(
          pointA.x,
          pointA.y,
          pointB.x,
          pointB.y,
          pointC.x,
          pointC.y,
          pointD.x,
          pointD.y
        );
        path.noStroke();
        path.fill = color;
        return path;
      }

      function drawWheel() {
        let previousAngle = 0;
        if (group) {
          previousAngle = group.rotation;
          destroyPaths();
        }

        const { width } = two;
        const numColors = COLORS.length;
        const rotationUnit = 2 * PI / words.length;
        const xOffset = width * ratios.tickerRadius * 2;
        const radius = (width - xOffset * 2) / 2;
        const center = {
          x: width / 2,
          y: radius + xOffset,
        };

        const wheelShadow = two.makeArcSegment(
          center.x - 8,
          center.y + 4,
          0,
          radius,
          0,
          2 * PI
        );

        wheelShadow.noStroke();
        wheelShadow.fill = '#9f9f9f';

        group = two.makeGroup();
        group.rotation = previousAngle;
        words.map((word, i, arr) => {
          const angle = rotationUnit * i - (PI + rotationUnit) / 2;
          const arc = two.makeArcSegment(
            center.x,
            center.y,
            0,
            radius,
            0,
            2 * PI / arr.length
          );
          arc.rotation = angle;
          arc.fill = COLORS[i % numColors];
          arc.linewidth = 3;

          const textVertex = {
            x:
              center.x +
              (radius - radius * ratios.edgeDist - radius * ratios.textSize()) / 2 *
              Math.cos(angle + rotationUnit / 2),
            y:
              center.y +
              (radius - radius * ratios.edgeDist - radius * ratios.textSize()) / 2 *
              Math.sin(angle + rotationUnit / 2),
          };

          const text = two.makeText(word, textVertex.x, textVertex.y, {
            'family': 'AvenirLTStd-Medium, sans-serif',
            'style': 'italic',
            'weight': 'bolder'
          });
          text.rotation = rotationUnit * i - PI / 2;
          text.alignment = 'center';
          text.fill = i % 2 ? '#464543' : '#f0efed';
          text.size = radius * ratios.textSize();

          return group.add(arc, text);
        });

        const wheelCenter = two.makeArcSegment(
          center.x,
          center.y,
          0,
          radius / 10,
          0,
          2 * PI
        );
        wheelCenter.fill = TICKER_COLOR;
        wheelCenter.linewidth = 3;
        group.add(wheelCenter);

        // const wheelCenterShadow = two.makeArcSegment(
        //   center.x,
        //   center.y,
        //   radius/10-2,
        //   radius/10,
        //   degToRad(-100),
        //   degToRad(80)
        // );
        // wheelCenterShadow.fill = '#9f9f9f';

        group.translation.set(center.x, center.y);
        group.center();
        drawTicker();

        two.update();
      }

      function handleResize() {
        setViewBox(two.width, two.height);
        drawWheel();
        drawTicker();
        two.update();
      }

      function handleCursorDown(e) {
        const groupElem = group._renderer.elem;

        isGroupActive = groupElem === e.target || groupElem.contains(e.target);
        curPosArr = isGroupActive ? curPosArr.concat(getEventPos(e)) : curPosArr;
        lastCurTime = performance.now();
      }

      function handleCursorMove(e) {
        if (isGroupActive && curPosArr.length) {
          e.preventDefault();
          lastCurTime = performance.now();
          curPosArr = curPosArr.concat(getEventPos(e));
          const currPos = curPosArr[curPosArr.length - 1];
          const prevPos = curPosArr[curPosArr.length - 2];
          const groupBounds = group._renderer.elem.getBoundingClientRect();
          const groupCenter = {
            x: groupBounds.left + groupBounds.width / 2,
            y: groupBounds.top + groupBounds.height / 2,
          };
          const angleAtCursorDown = Math.atan2(
            prevPos.y - groupCenter.y,
            prevPos.x - groupCenter.x
          );
          const angleAtCursorNow = Math.atan2(
            currPos.y - groupCenter.y,
            currPos.x - groupCenter.x
          );
          const deltaRotation = angleAtCursorNow - angleAtCursorDown;
          dirScalar = deltaRotation > 0 ? 1 : -1;

          group.rotation = (group.rotation + deltaRotation) % (2 * PI);

          handleRotationChange(group.rotation);

          two.update();
        }
      }

      function handleCursorUp(e) {
        if (isGroupActive && curPosArr.length > 1) {
          const currPos = getEventPos(e);
          const lastPos = curPosArr[curPosArr.length - 2];
          const timeNow = performance.now();
          const time = timeNow - lastCurTime;
          const distance = Math.sqrt(
            Math.pow(currPos.x - lastPos.x, 2) + Math.pow(currPos.y - lastPos.y, 2)
          );
          speed = Math.min(distance / time, maxSpeed);

          two.bind('update', animateWheel);
        }

        curPosArr = [];
        isGroupActive = false;
      }

      function getEventPos(e) {
        const { clientX: x, clientY: y } = getEvent(e);

        return { x, y };
      }

      function getEvent(e) {
        return e.changedTouches ? e.changedTouches[0] : e;
      }

      function animateWheel() {
        group.rotation = (group.rotation + speed * dirScalar) % (2 * PI);
        speed = speed * friction;

        handleRotationChange(group.rotation);

        if (speed < 0.005) {
          two.unbind('update', animateWheel);
          if (options.onWheelStop && typeof options.onWheelStop === 'function') {
            options.onWheelStop();
          }
        }
      }

      function handleRotationChange(angle) {
        if (options.onWheelTick && typeof options.onWheelTick === 'function') {
          options.onWheelTick(angle);
        }
      }

      function spin(newSpeed) {
        speed = newSpeed;
        two.bind('update', animateWheel);
      }

      function updateDims({ height, width }) {
        two.width = parseInt(width, 10);
        two.height = parseInt(height, 10);
        two.trigger('resize');
      }

      function getCurrentWord() {
        const numWords = words.length;
        const segmentAngle = (2 * PI) / numWords;
        const currAngle = ((5 * PI) / 2 - group.rotation + segmentAngle / 2) % (2 * PI);

        return words.find((_, i) => segmentAngle * (i + 1) > currAngle);
      }

      function initEvents() {
        const domElement = two.renderer.domElement;

        Object.keys(eventMap).map(type =>
          domElement.addEventListener(type, eventMap[type])
        );
      }

      function removeEvents() {
        const domElement = two.renderer.domElement;

        two.unbind('update');

        Object.keys(eventMap).map(type =>
          domElement.removeEventListener(type, eventMap[type])
        );
      }

      function destroyPaths() {
        group.remove.apply(group, group.children);
        two.clear();
      }

      function destroy() {
        destroyPaths();
        removeEvents();

        return true;
      }

      return {
        destroy,
        drawWheel,
        getCurrentWord,
        init,
        setWords,
        spin,
        updateDims
      };
    };

    // DOM
    const mount = document.querySelector('.js-mount');

    const wheel = wheelFactory(mount);
    wheel.init({
      width: Math.min(window.innerWidth, window.innerHeight),
      height: Math.min(window.innerWidth, window.innerHeight),
      onWheelTick: this.onWheelTick,
      onWheelStop: this.onWheelStop
    });
    wheel.setWords(this.getWords());
    wheel.drawWheel();

    window.addEventListener('resize', () => {
      wheel.updateDims({
        width: Math.min(window.innerWidth, window.innerHeight),
        height: Math.min(window.innerWidth, window.innerHeight),
      });
    });

    this.wheel = wheel;
  }

  private getWords = () => this.WORDS.map(wordInfo => wordInfo[this.lang].word);

  private onWheelTick = (angle) => {
    // console.log('tick');
  }

  private onWheelStop = () => {
    const index = this.WORDS.findIndex(wordInfo => wordInfo[this.lang].word === this.wheel.getCurrentWord());
    const dupIndex = this.wordIndexes.lastIndexOf(index);
    console.log('stop', dupIndex, this.wordIndexes);
    if (dupIndex !== -1 && dupIndex >= this.wordIndexes.length - 2) {
      this.spin();
    } else {
      this.wordIndexes.push(index);
      const audio = new Audio('assets/audio/wheel-stop.ogg');
      audio.play();
      // console.log('stop');

      this.modalRef = this.modalService.open(ResultsModalComponent, {
        backdropClass: 'light-brown-backdrop',
        centered: true
      });
      Object.assign(this.modalRef.componentInstance, {
        lang: this.lang,
        wordInfos: Object.assign([], this.wordIndexes) // make a copy because reverse() affects the original array 
          .reverse()
          .slice(0, 2)
          .map(wordIndex => this.WORDS[wordIndex][this.lang])
      });

      this.modalRef.result.then((result) => {
        console.log(`Closed with: ${result}`);
      }, (reason) => {
        console.log(`Dismissed ${this.getDismissReason(reason)}`);
      });
    }
  }

  spin = () => {
    this.wheel.spin(Math.random());
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnDestroy(): void {
    if (this.wheel) {
      this.wheel.destroy();
    }
    if (this.modalRef) {
      this.modalRef.dismiss();
    }
  }
}
