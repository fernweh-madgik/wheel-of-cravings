import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-results-modal',
  templateUrl: './results-modal.component.html',
  styleUrls: ['./results-modal.component.scss']
})
export class ResultsModalComponent {
  wordInfos: { word: string, text: string }[];
  lang: 'en' | 'ar';

  constructor(public modal: NgbActiveModal) { }

}
